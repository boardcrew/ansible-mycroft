
Install [Mycroft - Picroft](https://mycroft.ai/blog/major-picroft-update-moving-to-raspbian-stretch/)

Also is important to run `ssh-copy-id` to make your ssh key to be trusted by the RaspberryPI...

This playbook will configure `pulseaudio` and some sounde directives

Adjust the hosts file and then:
```
ansible-playbook -i hosts mycroft-prereqs.yml
```
